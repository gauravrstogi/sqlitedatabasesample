//
//  main.m
//  DatabaseSample
//
//  Created by Gaurav Rastogi on 16/02/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

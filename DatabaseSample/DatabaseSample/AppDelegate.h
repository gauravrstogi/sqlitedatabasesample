//
//  AppDelegate.h
//  DatabaseSample
//
//  Created by Gaurav Rastogi on 16/02/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


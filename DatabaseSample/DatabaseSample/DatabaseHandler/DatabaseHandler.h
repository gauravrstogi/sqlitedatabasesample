//
//  DatabaseHandler.h
//  DatabaseSample
//
//  Created by Gaurav Rastogi on 16/02/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface DatabaseHandler : NSObject
{
    NSString *databasePath;
}

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

+(DatabaseHandler *)getSharedInstance;

-(BOOL)checkAndCreateDataBase;

-(BOOL)saveData:(NSString*)registerNumber name:(NSString*)name department:(NSString*)department year:(NSString*)year;

-(NSMutableDictionary *)findByRegisterNumber:(NSString*)registerNumber;

@end

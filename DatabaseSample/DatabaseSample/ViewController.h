//
//  ViewController.h
//  DatabaseSample
//
//  Created by Gaurav Rastogi on 16/02/15.
//  Copyright (c) 2015 Gaurav Rastogi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DatabaseHandler/DatabaseHandler.h"

@interface ViewController : UIViewController
{
    IBOutlet UITextField *regNoTextField;
    IBOutlet UITextField *nameTextField;
    IBOutlet UITextField *departmentTextField;
    IBOutlet UITextField *yearTextField;
    IBOutlet UITextField *findByRegisterNumberTextField;
    IBOutlet UIScrollView *myScrollView;
}

-(IBAction)saveData:(id)sender;
-(IBAction)findData:(id)sender;

@end

